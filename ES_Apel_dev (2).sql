-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2014 at 06:16 PM
-- Server version: 5.5.34
-- PHP Version: 5.3.10-1ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ES_Apel_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_admin_comments`
--

CREATE TABLE IF NOT EXISTS `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resource_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `email`, `encrypted_password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `created_at`, `updated_at`) VALUES
(1, 'admin@example.com', '$2a$10$P3lXBvf/xi9J.1WvBCAOTeMf.t2sNc5ShvT27.qoVlkdnG8Y4MTwm', NULL, NULL, NULL, 2, '2013-12-24 03:07:59', '2013-12-23 05:59:19', '127.0.0.1', '127.0.0.1', '2013-12-23 05:58:17', '2013-12-24 03:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE IF NOT EXISTS `diseases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `handling` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`id`, `code`, `name`, `info`, `handling`, `created_at`, `updated_at`) VALUES
(1, 'P001', 'Kutu Hijau', NULL, NULL, NULL, NULL),
(2, 'P002', 'Tungau', NULL, NULL, NULL, NULL),
(3, 'P003', 'Thrips', NULL, NULL, NULL, NULL),
(4, 'P004', 'Ulat Daun', NULL, NULL, NULL, NULL),
(5, 'P005', 'Serangga Penghirup daun', NULL, NULL, NULL, NULL),
(6, 'P006', 'Ulat Daun Hitam', NULL, NULL, NULL, NULL),
(7, 'P007', 'Lalat Buah', NULL, NULL, NULL, NULL),
(8, 'P008', 'Embun Tepung', NULL, NULL, NULL, NULL),
(9, 'P009', 'Bercak Daun', NULL, NULL, NULL, NULL),
(10, 'P010', 'Jamjur Upas', NULL, NULL, NULL, NULL),
(11, 'P011', 'Kanker', NULL, NULL, NULL, NULL),
(12, 'P012', 'Busuk Buah', NULL, NULL, NULL, NULL),
(13, 'P013', 'Busuk Akar', NULL, NULL, NULL, NULL),
(26, 'P026', 'sssssaaaaa', 'sasasas', 'sasasasa', '2013-12-24 04:51:15', '2013-12-24 05:05:58'),
(27, 'P027', 'kggk', 'hjghjgh', 'ghjgh', '2013-12-24 10:46:38', '2013-12-24 10:46:38');

-- --------------------------------------------------------

--
-- Table structure for table `facts`
--

CREATE TABLE IF NOT EXISTS `facts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symptom_id` int(11) DEFAULT NULL,
  `disease_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symptom_id` int(11) DEFAULT NULL,
  `disease_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`id`, `symptom_id`, `disease_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 4, 7, NULL, NULL),
(6, 5, 1, NULL, NULL),
(7, 6, 2, NULL, NULL),
(8, 7, 2, NULL, NULL),
(9, 8, 2, NULL, NULL),
(10, 8, 5, NULL, NULL),
(11, 8, 12, NULL, NULL),
(12, 9, 3, NULL, NULL),
(13, 10, 3, NULL, NULL),
(14, 11, 3, NULL, NULL),
(15, 12, 3, NULL, NULL),
(16, 13, 3, NULL, NULL),
(17, 14, 4, NULL, NULL),
(18, 15, 4, NULL, NULL),
(19, 15, 9, NULL, NULL),
(20, 16, 4, NULL, NULL),
(21, 17, 5, NULL, NULL),
(22, 18, 5, NULL, NULL),
(23, 19, 5, NULL, NULL),
(24, 20, 6, NULL, NULL),
(25, 21, 7, NULL, NULL),
(26, 22, 8, NULL, NULL),
(27, 23, 8, NULL, NULL),
(28, 24, 8, NULL, NULL),
(29, 25, 8, NULL, NULL),
(30, 26, 8, NULL, NULL),
(31, 27, 9, NULL, NULL),
(32, 28, 9, NULL, NULL),
(33, 29, 9, NULL, NULL),
(34, 30, 11, NULL, NULL),
(35, 31, 11, NULL, NULL),
(36, 32, 11, NULL, NULL),
(37, 33, 11, NULL, NULL),
(38, 34, 12, NULL, NULL),
(39, 35, 11, NULL, NULL),
(40, 36, 11, NULL, NULL),
(41, 37, 11, NULL, NULL),
(42, 38, 11, NULL, NULL),
(43, 39, 11, NULL, NULL),
(44, 40, 12, NULL, NULL),
(45, 41, 12, NULL, NULL),
(46, 42, 12, NULL, NULL),
(47, 43, 12, NULL, NULL),
(48, 44, 13, NULL, NULL),
(49, 45, 13, NULL, NULL),
(50, 46, 13, NULL, NULL),
(52, 19, 26, '2013-12-24 07:55:08', '2013-12-24 07:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20131216104046'),
('20131216104238'),
('20131216104448'),
('20131216104509'),
('20131223055032'),
('20131223055034');

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE IF NOT EXISTS `symptoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symptom` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`id`, `code`, `symptom`, `created_at`, `updated_at`) VALUES
(1, 'G001', 'Daun berubah bentuk, berkerut dan mengeriting', NULL, NULL),
(2, 'G002', 'Pembungaan terhambat', NULL, NULL),
(3, 'G003', 'Buah-buahan muda gugur', NULL, NULL),
(4, 'G004', 'Kualitas bunga jelek', NULL, NULL),
(5, 'G005', 'Tanaman tidak menghasilkan buah', NULL, NULL),
(6, 'G006', 'Daun berbercak kuning, buram, coklat dan mengering', NULL, NULL),
(7, 'G007', 'Buah berbercak keperak-perakan', NULL, NULL),
(8, 'G008', 'Buah berbercak coklat', NULL, NULL),
(9, 'G009', 'Daun berbintik-bintik putih', NULL, NULL),
(10, 'G010', 'Kedua sisi daun menggulung ke atas', NULL, NULL),
(11, 'G011', 'Pertumbuhan dahun tidak normal', NULL, NULL),
(12, 'G012', 'Daun pada ujung tunas menjadi kering dan gugur', NULL, NULL),
(13, 'G013', 'Pada buah muda terdapat luka berwarna coklat keabu-abuan', NULL, NULL),
(14, 'G014', 'Daun tergerek', NULL, NULL),
(15, 'G015', 'Daun berlubang-lubang tidak teratur', NULL, NULL),
(16, 'G016', 'Daun tinggal tulang-tulangnya saja', NULL, NULL),
(17, 'G017', 'Daun berbercak coklat', NULL, NULL),
(18, 'G018', 'Perkembangan daun tidak simetris', NULL, NULL),
(19, 'G019', 'Tunas menjadi coklat, kering dan mati', NULL, NULL),
(20, 'G020', 'Tanaman tinggal tulang daun saja', NULL, NULL),
(21, 'G021', 'Buah terdapat benjol benjol', NULL, NULL),
(22, 'G022', 'Pada permukaan daun tampak tepung putih', NULL, NULL),
(23, 'G023', 'Tunas tumbuh tidak normal atau kerdil', NULL, NULL),
(24, 'G024', 'Buah Muda berwarna kecoklat-coklatan', NULL, NULL),
(25, 'G025', 'Kulit buah muda pecah berkutil-kutil coklat', NULL, NULL),
(26, 'G026', 'buah tua berwarna coklat muda, seperti buah sawo', NULL, NULL),
(27, 'G027', 'pada bercak permukaan atas timbul titik-titik berwarna hitam', NULL, NULL),
(28, 'G028', 'hampir seluruh daun berwarna coklat, nekrose kering', NULL, NULL),
(29, 'G029', 'sebagian daun gugur', NULL, NULL),
(30, 'G030', 'terdapat bintik-bintik putih pada permukaan kulit tanaman', NULL, NULL),
(31, 'G031', 'terdapat kerak berwarna merah jambu, setelah tua menjadi lebih muda atau putih', NULL, NULL),
(32, 'G032', 'kulit kayu dibawah kerak busuk dan kering', NULL, NULL),
(33, 'G033', 'terdapat bintik atau bulatan kecil berwarna pada sisi yang telah busuk dan kering', NULL, NULL),
(34, 'G034', 'batang atau cabang busuk', NULL, NULL),
(35, 'G035', 'batang atau cabang berwarna coklat kehitam-hitaman', NULL, NULL),
(36, 'G036', 'batang atau cabang kadang mengeluarkan cairan ', NULL, NULL),
(37, 'G037', 'buah berbercak kecil berwarna coklat muda', NULL, NULL),
(38, 'G038', 'buah berair atau mengembung', NULL, NULL),
(39, 'G039', 'warna buah menjadi pucat', NULL, NULL),
(40, 'G040', 'buah berbecak kecil berwarna kehijauan berbentuk bulat', NULL, NULL),
(41, 'G041', 'buah busuk', NULL, NULL),
(42, 'G042', 'terdapat bintik hitam', NULL, NULL),
(43, 'G043', 'terdapat bintik orange', NULL, NULL),
(44, 'G044', 'daun layu', NULL, NULL),
(45, 'G045', 'daun gugur', NULL, NULL),
(46, 'G046', 'kulit akar membusuk', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
