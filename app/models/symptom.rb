class Symptom < ActiveRecord::Base
	has_many :rules
	has_many :diseases, :through => :rules

	alias_attribute :name, :symptom

	after_create :generate_code

	def generate_code
		if self.id.to_s.length >= 4
			self.code = "G#{self.id}"
		elsif self.id.to_s.length >= 3
			self.code = "G0#{self.id}"
		elsif self.id.to_s.length >= 2
			self.code = "G00#{self.id}"
		elsif self.id.to_s.length >= 1
			self.code = "G000#{self.id}"
		end
		self.save
	end
end
