ActiveAdmin.register Symptom do
  permit_params :name, :disease_id

  index do
    column 'Kode', :code
    column 'Gejala', :symptom
    actions
  end
  
  filter :symptom, :label => 'Gejala'
  filter :diseases, :label => 'Penyakit'

  form do |f|
    f.inputs  do
      f.input :name, label: "Gejala"
    end
    f.actions
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
